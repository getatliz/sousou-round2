class Moneypot
    include Mongoid::Document
    include Mongoid::Timestamps
  
    field :name, type: String
    field :interval_amount, type: Integer
  
    def status
      "Deposit starts in 3 days"
    end
  
  end